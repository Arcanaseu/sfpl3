<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\AttributeOverrides({
 *    @ORM\AttributeOverride(name="username",
 *          column=@ORM\Column(
 *              nullable = false,
 *              unique   = false,
 *              length   = 255
 *          )
 *      )
 * })
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="username", type="string", length=255)
//     */
//    protected $username;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="usernameCanonical", type="string", length=255)
//     */
//    protected $usernameCanonical;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="email", type="string", length=255, unique=true)
//     */
//    protected $email;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="emailCanonical", type="string", length=255, unique=true)
//     */
//    protected $emailCanonical;
//
//    /**
//     * @var bool
//     *
//     * @ORM\Column(name="enabled", type="boolean")
//     */
//    protected $enabled;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="salt", type="string", length=255)
//     */
//    protected $salt;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="password", type="string", length=255)
//     */
//    protected $password;
//
//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="lastLogin", type="datetime", nullable=true)
//     */
//    protected $lastLogin;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="confirmationToken", type="string", length=255)
//     */
//    protected $confirmationToken;
//
//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="passwordRequestedAt", type="datetime")
//     */
//    protected $passwordRequestedAt;

//    /**
//     * @ORM\ManyToMany(targetEntity="UserGroup", inversedBy="users")
//     * @ORM\JoinTable(name="users_groups")
//     */
//    protected $groups;

//    /**
//     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Role", inversedBy="users")
//     * @ORM\JoinTable(name="users_roles")
//     */
//    protected $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=255, nullable=true)
     */
    protected $apiKey;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }
}
